//
//  CoreDataManagerTests.swift
//  SherpaTechTaskTests
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import XCTest
@testable import SherpaTechTask

class CoreDataManagerTests: XCTestCase {
    
    private var mockCoreDataManager = CoreDataManager(appDelegate: MockAppDelegate())
    
    override func tearDown() {
        print("TearDown")
        mockCoreDataManager = CoreDataManager(appDelegate: MockAppDelegate())
    }
    
    func testInsertUser() {
        let name = "Alice"
        let postcode = "12345"
        let city = "Wonderland"
        let user = User(name: name, postcode: postcode, city: city)
        
        let insertResult = mockCoreDataManager.insertUser(name: name, postcode: postcode, placeNames: city)
        
        XCTAssert(insertResult)
        XCTAssert(mockCoreDataManager.fetchAll() == [user])
    }
    
    func testFetchAll() {
        let user0 = User(name: "Alice", postcode: "12345", city: "Wonderland")
        let user1 = User(name: "Bob", postcode: "67890", city: "Testland")
        let user2 = User(name: "Carol", postcode: "48001", city: "Bilbao")
        
        let _ = mockCoreDataManager.insertUser(name: user0.name!, postcode: user0.postcode!, placeNames: user0.city!)
        let _ = mockCoreDataManager.insertUser(name: user1.name!, postcode: user1.postcode!, placeNames: user1.city!)
        let _ = mockCoreDataManager.insertUser(name: user2.name!, postcode: user2.postcode!, placeNames: user2.city!)
        
        let allUsers = mockCoreDataManager.fetchAll()
        
        XCTAssert(allUsers == [user0, user1, user2])
    }

}
