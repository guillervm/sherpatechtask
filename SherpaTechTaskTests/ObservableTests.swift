//
//  ObservableTests.swift
//  SherpaTechTaskTests
//
//  Created by Guillermo Velez de Mendizabal on 20/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import XCTest
@testable import SherpaTechTask

class ObservableTests: XCTestCase {
    
    func testObservableWithoutDefaultValue() {
        let observable = Observable<Int>()
        XCTAssert(observable.value == nil)
    }
    
    func testObservableWithDefaultValue() {
        let observable = Observable<Int>(initialValue: 13)
        XCTAssert(observable.value == 13)
    }
    
    func testObservableObserverNotified() {
        var value = 5
        let observable = Observable<Int>(initialValue: value)
        observable.observe { (newValue) in
            value = newValue
        }
        observable.value = 13
        XCTAssert(value == 13)
    }
    
    func testObservableManyObserversNotified() {
        var notificationsCount = 0
        let observable = Observable<Int>()
        for _ in 0..<3 {
            observable.observe { (value) in
                notificationsCount += 1
            }
        }
        observable.value = 13
        XCTAssert(notificationsCount == 3)
    }
    
}
