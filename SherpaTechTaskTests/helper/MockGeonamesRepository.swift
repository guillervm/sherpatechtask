//
//  MockGeonamesRepository.swift
//  SherpaTechTaskTests
//
//  Created by Guillermo Velez de Mendizabal on 20/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation
@testable import SherpaTechTask

class MockGeonamesRepository: GeonamesRepository {
    
    override func postalCodeSearch(postcode: String, onSuccess: @escaping (PostalCodeSearchResponse?) -> Void, onError: @escaping (Error) -> Void) {
        onSuccess(PostalCodeSearchResponse(postalCodes: [PostalCode(postalCode: postcode, lat: 51.50100893647978, lng: -0.14158760012261312, placeName: "London", countryCode: "GB", adminName1: "England", adminName2: "Greater London", adminName3: "City of Westminster London Boro", adminCode1: "ENG", adminCode2: "", adminCode3: "E09000033")]))
        
    }
    
}
