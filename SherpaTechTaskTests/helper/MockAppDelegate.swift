//
//  MockAppDelegate.swift
//  SherpaTechTaskTests
//
//  Created by Guillermo Velez de Mendizabal on 20/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation
import CoreData
@testable import SherpaTechTask

class MockAppDelegate: AppDelegate {
    
    override init() {
        super.init()
        self.persistentContainer = {
            let container = NSPersistentContainer(name: NSPersistentContainer.ContainerUserData)
            let description = NSPersistentStoreDescription()
            description.type = NSInMemoryStoreType
            description.shouldAddStoreAsynchronously = false
            
            container.persistentStoreDescriptions = [description]
            container.loadPersistentStores { (description, error) in
                precondition( description.type == NSInMemoryStoreType )
                
                if let error = error {
                    fatalError("Create an in-mem coordinator failed \(error)")
                }
            }
            return container
        }()
    }
    
}
