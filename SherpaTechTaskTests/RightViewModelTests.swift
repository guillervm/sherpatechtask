//
//  RightViewModelTests.swift
//  SherpaTechTaskTests
//
//  Created by Guillermo Velez de Mendizabal on 20/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import XCTest
@testable import SherpaTechTask

class RightViewModelTests: XCTestCase {
    
    private let mockCoreDataManager = CoreDataManager(appDelegate: MockAppDelegate())
    private let mockGeonamesRepository = MockGeonamesRepository()
    private var rightViewModel: RightViewModel!
    
    override func setUp() {
        rightViewModel = RightViewModel.init(geonamesRepository: mockGeonamesRepository, coreDataManager: mockCoreDataManager)
    }
    
    func testProcessNameAndPostcode() {
        let name = "Bob"
        let postcode = "SW1A 1AA"
        let city = "London"
        let user = User(name: name, postcode: postcode, city: city)
        
        rightViewModel.processNameAndPostcode(name: name, postcode: postcode)
        
        XCTAssert(rightViewModel.output.value == "{\n  \"city\" : \"\(city)\",\n  \"name\" : \"\(name)\",\n  \"postcode\" : \"\(postcode)\"\n}")
        XCTAssert(rightViewModel.operationResultSuccess.value == true)
        XCTAssert(mockCoreDataManager.fetchAll() == [user])
    }
    
}
