//
//  ColourExtensions.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {

    @nonobjc class var dark: UIColor {
        return UIColor(red: 18.0 / 255.0, green: 10.0 / 255.0, blue: 21.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var niceBlue: UIColor {
        return UIColor(red: 0.0, green: 111.0 / 255.0, blue: 226.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var niceBlueDark: UIColor {
        return UIColor(red: 0.0, green: 108.0 / 255.0, blue: 202.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var coral: UIColor {
        return UIColor(red: 1.0, green: 122.0 / 255.0, blue: 122.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var coralDark: UIColor {
        return UIColor(red: 1.0, green: 101.0 / 255.0, blue: 101.0 / 255.0, alpha: 1.0)
    }

}
