//
//  Extensions.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension UIView {
    
    func roundCorners(radius: CGFloat) {
        layer.cornerRadius = radius
    }
    
    func border(width: CGFloat, colour: UIColor) {
        layer.borderWidth = width
        layer.borderColor = colour.cgColor
    }
    
}

extension Date {
    
    func timeMillisSince1970() -> Int {
        return Int(timeIntervalSince1970 * 1000.0)
    }
    
}

extension UserDefaults {
    
    static var KeyHelloCount = "hello_count"
    
}

extension NSPersistentContainer {
    
    static var ContainerUserData = "UserData"
    
}

extension NSEntityDescription {
    
    static var EntityUserMaster = "UserMaster"
    static var EntityUserDetails = "UserDetails"
    
}

extension NSManagedObject {
    
    static var KeyUserMasterName = "name"
    static var KeyUserDetailsCity = "city"
    static var KeyUserDetailsPostcode = "postcode"
    
}
