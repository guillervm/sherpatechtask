//
//  CoreDataManager.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 20/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CoreDataManager {
    
    private let appDelegate: AppDelegate?
    
    init(appDelegate: AppDelegate?) {
        self.appDelegate = appDelegate
    }
    
    func insertUser(name: String, postcode: String, placeNames: String) -> Bool {
        if let managedContext = appDelegate?.persistentContainer.viewContext {
            if let userMasterEntity = NSEntityDescription.entity(forEntityName: NSEntityDescription.EntityUserMaster, in: managedContext), let userDetailsEntity = NSEntityDescription.entity(forEntityName: NSEntityDescription.EntityUserDetails, in: managedContext) {
                let user = NSManagedObject(entity: userMasterEntity, insertInto: managedContext)
                user.setValue(name, forKey: NSManagedObject.KeyUserMasterName)
                let details = NSManagedObject(entity: userDetailsEntity, insertInto: managedContext)
                details.setValue(postcode, forKey: NSManagedObject.KeyUserDetailsPostcode)
                details.setValue(placeNames, forKey: NSManagedObject.KeyUserDetailsCity)
                (user as! UserMaster).details = details as? UserDetails
                do {
                    if managedContext.hasChanges {
                        try managedContext.save()
                    }
                    return true
                } catch let error {
                    print("Error: \(error)")
                }
            }
            
        }
        return false
    }
    
    func fetchAll() -> [User] {
        if let managedContext = appDelegate?.persistentContainer.viewContext {
            let fetchRequest: NSFetchRequest<UserMaster> = UserMaster.fetchRequest()
            let sortDescriptor = NSSortDescriptor(key: #keyPath(UserMaster.name), ascending: true)
            fetchRequest.sortDescriptors = [sortDescriptor]
            let results = try? managedContext.fetch(fetchRequest)
            if let results = results {
                return results.map { (userMaster) -> User in
                    return User(name: userMaster.name, postcode: userMaster.details?.postcode, city: userMaster.details?.city)
                }
            }
        }
        return []
    }
    
}
