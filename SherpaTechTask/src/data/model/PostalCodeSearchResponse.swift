//
//  PostalCodeSearchResponse.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation

struct PostalCodeSearchResponse: Codable {
    
    var postalCodes: [PostalCode]
    
}
