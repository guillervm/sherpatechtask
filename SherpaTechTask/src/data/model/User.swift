//
//  User.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 20/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation

struct User: Codable, Equatable {
    
    var name: String?
    var postcode: String?
    var city: String?
    
    static func == (lhs: User, rhs: User) -> Bool {
        return lhs.name == rhs.name && lhs.postcode == rhs.postcode && lhs.city == rhs.city
    }
}
