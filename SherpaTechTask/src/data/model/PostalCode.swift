//
//  PostalCode.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation

struct PostalCode: Codable {
    
    let postalCode: String
    let lat: Double
    let lng: Double
    let placeName: String
    let countryCode: String
    let adminName1: String?
    let adminName2: String?
    let adminName3: String?
    let adminCode1: String?
    let adminCode2: String?
    let adminCode3: String?
    
}
