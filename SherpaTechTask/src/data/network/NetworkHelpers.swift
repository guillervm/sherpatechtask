//
//  Request.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation

protocol RequestType {
    
    associatedtype ResponseType: Codable
    
    var data: Request { get }
    
}

extension RequestType {
    
    func doRequest(onSuccess: @escaping (ResponseType?) -> Void, onError: @escaping (Error) -> Void) {
        SingletonNetworkManager.instance.doRequest(request: self.data, onSuccess: { (responseData) in
            if let responseData = responseData {
                do {
                    let result = try JSONDecoder().decode(ResponseType.self, from: responseData)
                    DispatchQueue.main.async {
                        onSuccess(result)
                    }
                } catch {
                    DispatchQueue.main.async {
                        onError(CustomError.parsingError)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    onSuccess(nil)
                }
            }
        }) { (error) in
            DispatchQueue.main.async {
                onError(error)
            }
        }
    }
    
}

enum CustomError: Swift.Error {
    case invalidURL
    case genericError
    case parsingError
}

struct Request {
    
    enum HTTPMethod: String {
        case get
        case post
        case put
        case delete
    }
    
    let path: String
    let method: HTTPMethod
    let queryParams: [String: String]?
    
    init(path: String, method: HTTPMethod, queryParams: [String: String]? = nil) {
        self.path = path
        self.method = method
        self.queryParams = queryParams
    }
    
}
