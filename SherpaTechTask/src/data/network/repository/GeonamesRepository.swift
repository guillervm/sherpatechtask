//
//  GeonamesRepository.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation

class GeonamesRepository {
    
    func postalCodeSearch(postcode: String, onSuccess: @escaping (PostalCodeSearchResponse?) -> Void, onError: @escaping (Error) -> Void) {
        PostalCodeSearch(postcode: postcode).doRequest(onSuccess: onSuccess, onError: onError)
    }
    
    private static func getBaseQueryParams() -> [String : String] {
        return ["username": "guillervm"]
    }
    
    private class PostalCodeSearch: RequestType {
        
        typealias ResponseType = PostalCodeSearchResponse
        
        var data: Request
        
        init(postcode: String) {
            var queryParams = getBaseQueryParams()
            queryParams["postalcode"] = postcode
            self.data = Request(path: "http://api.geonames.org/postalCodeSearchJSON", method: .get, queryParams: queryParams)
        }
        
    }
    
}
