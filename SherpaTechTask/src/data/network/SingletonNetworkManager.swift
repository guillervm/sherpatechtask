//
//  SingletonNetworkManager.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation

class SingletonNetworkManager {
    
    static let instance = SingletonNetworkManager()
    
    private init() {
        //
    }
    
    func doRequest(request: Request, onSuccess: @escaping (Data?) -> Void, onError: @escaping (Error) -> Void) {
        
        var urlComponents = URLComponents(string: request.path)
        if let queryParams = request.queryParams {
            urlComponents?.queryItems = queryParams.map { (key, value) in
                URLQueryItem(name: key, value: value)
            }
        }
        if let percentEncodedQuery = urlComponents?.percentEncodedQuery {
            urlComponents?.percentEncodedQuery = percentEncodedQuery.replacingOccurrences(of: "+", with: "%2B")
        }
        if let url = urlComponents?.url {
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = request.method.rawValue
            let dataTask = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
                if let error = error {
                    onError(error)
                } else if let response = response as? HTTPURLResponse {
                    if (200..<300) ~= response.statusCode {
                        onSuccess(data)
                    } else {
                        onError(CustomError.genericError)
                    }
                } else {
                    onError(CustomError.genericError)
                }
            }
            dataTask.resume()
        } else {
            onError(CustomError.invalidURL)
        }
        
    }
    
}
