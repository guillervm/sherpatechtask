//
//  LandingViewController.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import UIKit

class LandingViewController: BaseViewController {
    
    @IBOutlet weak var labelTitle: UILabel!
    
    private var landingViewModel: LandingViewModel
    
    override init(delegate: ViewController, viewModels: [BaseViewModel]? = nil) {
        landingViewModel = LandingViewModel()
        super.init(delegate: delegate, viewModels: [landingViewModel])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        Bundle.main.loadNibNamed("LandingViewController", owner: self, options: nil)
        
        let originOrigin = labelTitle.frame.origin
        labelTitle.frame.origin = CGPoint.init(x: view.frame.width, y: view.frame.height)
        UIView.animate(withDuration: 1) {
            self.labelTitle.frame.origin.x = originOrigin.x
            self.labelTitle.frame.origin.y = originOrigin.y
        }
        
        landingViewModel.shouldLaunch.observe(observer: { [weak self] (shouldLaunch) in
            if shouldLaunch {
                self?.navigationDelegate.toScreen(.main, animation: .fade)
            }
        })
        
        landingViewModel.startLaunchTimer()
    }

}
