//
//  MainViewController.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import UIKit

class MainViewController: BaseViewController, ButtonWrapperDelegate {
    
    @IBOutlet weak var labelLatLong: UILabel!
    @IBOutlet weak var labelSpeak: UILabel!
    @IBOutlet weak var buttonSpeak: UIButton!
    @IBOutlet weak var labelEnterAddress: UILabel!
    @IBOutlet weak var buttonEnterAddress: UIButton!
    
    private var mainViewModel: MainViewModel
    private var speakButtonWrapper: ButtonWrapper?
    private var enterAddressButtonWrapper: ButtonWrapper?
    
    override init(delegate: ViewController, viewModels: [BaseViewModel]? = nil) {
        mainViewModel = MainViewModel()
        super.init(delegate: delegate, viewModels: [mainViewModel])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        Bundle.main.loadNibNamed("MainViewController", owner: self, options: nil)
        
        speakButtonWrapper = ButtonWrapper(button: buttonSpeak, viewToUpdate: labelSpeak, colorNormal: UIColor.niceBlue.cgColor, colorPressed: UIColor.niceBlueDark.cgColor)
        speakButtonWrapper?.delegate = self
        
        labelSpeak.roundCorners(radius: 50.0)
        labelSpeak.border(width: 4.0, colour: .white)
        
        enterAddressButtonWrapper = ButtonWrapper(button: buttonEnterAddress, viewToUpdate: labelEnterAddress, colorNormal: UIColor.coral.cgColor, colorPressed: UIColor.coralDark.cgColor)
        enterAddressButtonWrapper?.delegate = self
        
        mainViewModel.location.observe { (location) in
            self.labelLatLong.text = "Lat: \(location.coordinate.latitude)\nLong: \(location.coordinate.longitude)"
        }
        mainViewModel.speechTranscription.observe { (speechTranscription) in
            if "hello".caseInsensitiveCompare(speechTranscription) == .orderedSame {
                self.mainViewModel.increseHelloCount()
            }
        }
        mainViewModel.speechRecognitionRunning.observe { (speechRecognitionRunning) in
            self.labelSpeak.text = speechRecognitionRunning ? "Stop" : "Speak"
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        mainViewModel.startLocationUpdates()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        mainViewModel.stopLocationUpdates()
    }
    
    // ButtonWrapperDelegate
    
    func buttonClicked(button: UIButton) {
        if button == buttonSpeak {
            mainViewModel.toogleSpeechRecognition()
        } else if button == buttonEnterAddress {
            navigationDelegate.toScreen(.right, animation: .slideLeft)
        }
    }
    
}
