//
//  RightViewController.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import UIKit

class RightViewController: BaseViewController, ButtonWrapperDelegate {
    
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFieldPostcode: UITextField!
    @IBOutlet weak var labelSend: UILabel!
    @IBOutlet weak var buttonSend: UIButton!
    @IBOutlet weak var labelOutput: UILabel!
    
    private var rightViewModel: RightViewModel
    private var sendButtonWrapper: ButtonWrapper?
    
    override init(delegate: ViewController, viewModels: [BaseViewModel]? = nil) {
        rightViewModel = RightViewModel()
        super.init(delegate: delegate, viewModels: [rightViewModel])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        Bundle.main.loadNibNamed("RightViewController", owner: self, options: nil)
        
        textFieldName.addTarget(self, action: #selector(nameOrPostcodeDidChange(_:)), for: .editingChanged)
        textFieldPostcode.addTarget(self, action: #selector(nameOrPostcodeDidChange(_:)), for: .editingChanged)
        
        sendButtonWrapper = ButtonWrapper(button: buttonSend, viewToUpdate: labelSend, colorNormal: UIColor.niceBlue.cgColor, colorPressed: UIColor.niceBlueDark.cgColor)
        sendButtonWrapper?.delegate = self
        sendButtonWrapper?.enabled = false
        
        rightViewModel.output.observe { [weak self] (output) in
            self?.labelOutput.text = output
        }
        rightViewModel.operationResultSuccess.observe { [weak self] (operationResultSuccess) in
            let alert = UIAlertController(title: "Operation result", message: operationResultSuccess ? "OK" : "KO", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self?.present(alert, animated: true, completion: nil)
        }
    }

    // ButtonWrapperDelegate
    
    func buttonClicked(button: UIButton) {
        if let name = textFieldName.text, let postcode = textFieldPostcode.text {
            if !name.isEmpty && !postcode.isEmpty {
                labelOutput.text = ""
                rightViewModel.processNameAndPostcode(name: name, postcode: postcode)
            }
        }
    }
    
    // Name and postcode text change selector
    
    @objc func nameOrPostcodeDidChange(_ textField: UITextField) {
        sendButtonWrapper?.enabled = !(textFieldName.text ?? "").isEmpty && !(textFieldPostcode.text ?? "").isEmpty
    }
    
}
