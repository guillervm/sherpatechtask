//
//  ButtonWrapper.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation
import UIKit

protocol ButtonWrapperDelegate: class {
    
    func buttonClicked(button: UIButton)
    
}

class ButtonWrapper {
    
    private static let scalePressed = 0.95
    private static let alphaDisabled: CGFloat = 0.5
    
    weak var delegate: ButtonWrapperDelegate?
    
    private let debouncer = Debouncer()
    private var colorNormal: CGColor
    private var colorPressed: CGColor
    private var button: UIButton
    private var viewToUpdate: UIView?
    var enabled = false {
        didSet {
            button.isUserInteractionEnabled = enabled
            viewToUpdate?.alpha = enabled ? 1.0 : ButtonWrapper.alphaDisabled
        }
    }
    
    init(button: UIButton, viewToUpdate: UIView? = nil, colorNormal: CGColor, colorPressed: CGColor) {
        self.button = button
        self.viewToUpdate = viewToUpdate
        self.colorNormal = colorNormal
        self.colorPressed = colorPressed
        button.addTarget(self, action: #selector(touchDown), for: .touchDown)
        button.addTarget(self, action: #selector(touchUp), for: .touchUpOutside)
        button.addTarget(self, action: #selector(touchUp), for: .touchCancel)
        button.addTarget(self, action: #selector(touchUpInside), for: .touchUpInside)
        
        // Initial state
        viewToUpdate?.layer.cornerRadius = 8.0
        viewToUpdate?.layer.backgroundColor = colorNormal
    }
 
    // Button send selectors
    
    @objc func touchDown() {
        viewToUpdate?.transform = CGAffineTransform.init(scaleX: 0.95, y: 0.95)
        viewToUpdate?.layer.backgroundColor = colorPressed
    }
    
    @objc func touchUp() {
        viewToUpdate?.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
        viewToUpdate?.layer.backgroundColor = colorNormal
    }
    
    @objc func touchUpInside() {
        touchUp()
        if debouncer.action() {
            delegate?.buttonClicked(button: button)
        }
    }
    
}
