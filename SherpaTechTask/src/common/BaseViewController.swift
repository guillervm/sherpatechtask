//
//  BaseViewController.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    
    var navigationDelegate: NavigationDelegate
    var loaderDelegate: LoaderDelegate
    var visibleLoadersCount = 0
    
    init(delegate: ViewController, viewModels: [BaseViewModel]? = []) {
        navigationDelegate = delegate
        loaderDelegate = delegate
        
        super.init(nibName: nil, bundle: nil)
        
        if let viewModels = viewModels {
            for viewModel in viewModels {
                viewModel.loader.observe(observer: { [unowned self] (visible) in
                    self.visibleLoadersCount += visible ? 1 : -1
                    if self.visibleLoadersCount == 0 {
                        self.loaderDelegate.hideLoader()
                    } else if self.visibleLoadersCount == 1 {
                        self.loaderDelegate.showLoader()
                    }
                })
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
