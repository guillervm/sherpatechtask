//
//  BaseViewModel.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation

class BaseViewModel: NSObject {
    
    var loader = Observable<Bool>(initialValue: false)
    
}
