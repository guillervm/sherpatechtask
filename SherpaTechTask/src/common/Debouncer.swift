//
//  Debouncer.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation

class Debouncer {
    
    static private let debounceTimeMillis = 500
    private var lastAction = 0
    
    func action() -> Bool {
        if Date().timeMillisSince1970() > lastAction + Debouncer.debounceTimeMillis {
            lastAction = Date().timeMillisSince1970()
            return true
        }
        return false
    }
    
}
