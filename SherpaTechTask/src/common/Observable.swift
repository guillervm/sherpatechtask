//
//  Observable.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation

// This class implements an Observable. Swift Bond or RxSwift could be used, but they're big and heavy for such an small project, so I find pointless adding such dependencies.
class Observable<T> {
    
    typealias Observer = (_ observable: T) -> Void
    
    private var observers = [Observer]()
    
    var value: T? {
        didSet {
            if let value = value {
                notifyObservers(value: value)
            }
        }
    }
    
    init(initialValue: T? = nil) {
        if let initialValue = initialValue {
            value = initialValue
        }
    }
    
    func observe(observer: @escaping Observer) {
        observers.append(observer)
    }
    
    private func notifyObservers(value: T) {
        for observer in observers {
            observer(value)
        }
    }
    
}
