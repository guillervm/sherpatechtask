//
//  RightViewModel.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation
import UIKit

class RightViewModel: BaseViewModel {
    
    private let geonamesRepository: GeonamesRepository
    private let coreDataManager: CoreDataManager
    var output = Observable<String>()
    var operationResultSuccess = Observable<Bool>()
    
    init(geonamesRepository: GeonamesRepository = GeonamesRepository(), coreDataManager: CoreDataManager = CoreDataManager(appDelegate: UIApplication.shared.delegate as? AppDelegate)) {
        self.geonamesRepository = geonamesRepository
        self.coreDataManager = coreDataManager
        super.init()
    }
    
    func processNameAndPostcode(name: String, postcode: String) {
        loader.value = true
        doPostalCodeSearch(name: name, postcode: postcode)
    }
    
    private func doPostalCodeSearch(name: String, postcode: String) {
        self.geonamesRepository.postalCodeSearch(postcode: postcode, onSuccess: { (postalCodeSearchResponse) in
            var placeNames = ""
            if let postalCodes = postalCodeSearchResponse?.postalCodes {
                for postalCode in postalCodes {
                    if !placeNames.isEmpty {
                        placeNames.append("|")
                    }
                    placeNames.append(postalCode.placeName)
                }
            }
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: ["name": name, "postcode": postcode, "city": placeNames], options: [.prettyPrinted, .sortedKeys])
                self.output.value = String.init(data: jsonData, encoding: .utf8)
                self.storeData(name: name, postcode: postcode, placeNames: placeNames)
            } catch let error {
                print("Error: \(error)")
                self.operationResultSuccess.value = false
                self.loader.value = false
            }
        }, onError: { (error) in
            print("Error: \(error)")
            self.operationResultSuccess.value = false
            self.loader.value = false
        })
    }
    
    private func storeData(name: String, postcode: String, placeNames: String) {
        self.operationResultSuccess.value = coreDataManager.insertUser(name: name, postcode: postcode, placeNames: placeNames)
        self.loader.value = false
    }
    
}
