//
//  MainViewModel.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation
import MapKit
import Speech

class MainViewModel: BaseViewModel, CLLocationManagerDelegate {
    
    var location = Observable<CLLocation>()
    var speechTranscription = Observable<String>()
    var speechRecognitionRunning = Observable<Bool>()
    
    private var lastLocation: CLLocation?
    private var locationManager: CLLocationManager?
    private var locationTimer: Timer?
    
    private let audioEngine = AVAudioEngine()
    private let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer(locale: Locale.init(identifier: "en_GB")) // As we would like to recognise the word "Hello" this is set to English for better matching
    private var request: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    
    func startLocationUpdates() {
        let authorizationStatus = CLLocationManager.authorizationStatus()
        if authorizationStatus != CLAuthorizationStatus.denied {
            if locationManager == nil {
                locationManager = CLLocationManager.init()
                locationManager?.desiredAccuracy = kCLLocationAccuracyBest
                locationManager?.delegate = self
                
                if authorizationStatus == CLAuthorizationStatus.notDetermined {
                    locationManager?.requestWhenInUseAuthorization()
                }
            }
            locationManager?.startUpdatingLocation()
            locationTimer?.invalidate()
            locationTimer = Timer.scheduledTimer(withTimeInterval: 10.0, repeats: true, block: { (timer) in
                self.updateLocation()
            })
        }
    }
    
    func stopLocationUpdates() {
        locationTimer?.invalidate()
        locationManager?.stopUpdatingLocation()
    }
    
    func toogleSpeechRecognition() {
        if recognitionTask == nil || recognitionTask!.isFinishing {
            let node = audioEngine.inputNode
            let recordingFormat = node.outputFormat(forBus: 0)
            request = SFSpeechAudioBufferRecognitionRequest()
            node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, _) in
                self.request?.append(buffer)
            }
            audioEngine.prepare()
            do {
                try audioEngine.start()
            } catch {
                return
            }
            if speechRecognizer == nil || !speechRecognizer!.isAvailable || request == nil {
                return
            }
            recognitionTask = speechRecognizer?.recognitionTask(with: request!, resultHandler: { (result, error) in
                if let result = result {
                    if result.isFinal {
                        self.speechTranscription.value = result.bestTranscription.formattedString
                    }
                } else if let error = error {
                    print(error)
                }
            })
            speechRecognitionRunning.value = true
        } else {
            recognitionTask?.finish()
            audioEngine.stop()
            audioEngine.inputNode.removeTap(onBus: 0)
            request?.endAudio()
            speechRecognitionRunning.value = false
        }
    }
    
    func increseHelloCount() {
        let preferences = UserDefaults.standard
        var count = preferences.integer(forKey: UserDefaults.KeyHelloCount)
        count += 1
        preferences.set(count, forKey: UserDefaults.KeyHelloCount)
        preferences.didChangeValue(forKey: UserDefaults.KeyHelloCount)
    }
    
    private func updateLocation() {
        if let validLocation = lastLocation {
            location.value = validLocation
        }
    }
    
    // CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !locations.isEmpty {
            lastLocation = locations.last
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.denied || status == CLAuthorizationStatus.notDetermined {
            stopLocationUpdates()
        } else {
            startLocationUpdates()
        }
    }
    
}

