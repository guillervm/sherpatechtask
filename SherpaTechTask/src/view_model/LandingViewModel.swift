//
//  LandingViewModel.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation

class LandingViewModel: BaseViewModel {
    
    private let secondsToLaunch = 5.0
    var shouldLaunch = Observable<Bool>(initialValue: false)
    
    func startLaunchTimer() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + secondsToLaunch) {
            self.shouldLaunch.value = true
        }
    }
    
}

