//
//  ViewController.swift
//  SherpaTechTask
//
//  Created by Guillermo Velez de Mendizabal on 19/01/2019.
//  Copyright © 2019 Guillermo Velez de Mendizabal. All rights reserved.
//

import Foundation
import UIKit

protocol NavigationDelegate {
    
    func toScreen(_ screen: ViewController.Screen)
    
    func toScreen(_ screen: ViewController.Screen, animation: ViewController.Animation)
    
}

protocol LoaderDelegate {
    
    func showLoader()
    
    func hideLoader()
    
}

class ViewController: UIViewController, NavigationDelegate, LoaderDelegate {
    
    enum Screen {
        case main
        case right
    }
    
    enum Animation {
        case none
        case fade
        case slideLeft
    }
    
    private static let animationDuration = 0.5
    private var current: UIViewController?
    private var loaderView: UIView?
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        current = LandingViewController(delegate: self)
        if let current = current {
            addChild(current)
            current.view.frame = view.bounds
            view.addSubview(current.view)
            current.didMove(toParent: self)
        }
    }
    
    private func doScreenTransition(new: BaseViewController, animation: Animation) {
        addChild(new)
        new.view.frame = view.bounds
        view.addSubview(new.view)
        new.didMove(toParent: self)
        if let current = current {
            switch animation {
                case .fade:
                    new.view.alpha = 0.0
                    UIView.animate(withDuration: ViewController.animationDuration, animations: {
                        new.view.alpha = 1.0
                    }) { (finished) in
                        self.finishScreenTransition(new: new)
                    }
                case .slideLeft:
                    new.view.frame.origin.x = current.view.frame.width
                    UIView.animate(withDuration: ViewController.animationDuration, animations: {
                        new.view.frame.origin.x = 0
                        current.view.frame.origin.x = -current.view.frame.width
                    }) { (finished) in
                        self.finishScreenTransition(new: new)
                    }
                default:
                    finishScreenTransition(new: new)
            }
        } else {
            finishScreenTransition(new: new)
        }
    }
    
    private func finishScreenTransition(new: BaseViewController) {
        if let current = current {
            current.willMove(toParent: nil)
            current.view.removeFromSuperview()
            current.removeFromParent()
        }
        current = new
    }
    
    // NavigationDelegate
    
    func toScreen(_ screen: ViewController.Screen) {
        toScreen(screen, animation: .none)
    }
    
    func toScreen(_ screen: ViewController.Screen, animation: ViewController.Animation) {
        var new: BaseViewController
        switch screen {
            case .main:
                new = MainViewController(delegate: self)
            case .right:
                new = RightViewController(delegate: self)
        }
        doScreenTransition(new: new, animation: animation)
    }
    
    // LoaderDelegate
    
    func showLoader() {
        if loaderView == nil {
            loaderView = UIView.init(frame: view.frame)
            loaderView?.backgroundColor = UIColor.black.withAlphaComponent(0.8)
            loaderView?.layer.zPosition = .greatestFiniteMagnitude
            let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
            activityIndicator.center = CGPoint.init(x: loaderView!.frame.width / 2, y: loaderView!.frame.height / 2)
            activityIndicator.startAnimating()
            loaderView?.addSubview(activityIndicator)
        }
        view.addSubview(loaderView!)
    }
    
    func hideLoader() {
        if let loaderView = loaderView {
            loaderView.removeFromSuperview()
        }
    }
    
}
